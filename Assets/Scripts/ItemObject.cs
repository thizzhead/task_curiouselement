using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class ItemObject : ScriptableObject
{
    public static int itemCount;
    public int id;
    public string title;
    [TextArea(10,15)]
    public string description;
    public Sprite icon;

    public void Awake()
    {
        id = itemCount++;
    }
}
