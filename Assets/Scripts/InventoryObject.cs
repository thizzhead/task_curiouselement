using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InventoryObject : ScriptableObject
{
    public int maxInventoryItems = 10;

    [SerializeField]
    private List<ItemObject> items = new List<ItemObject>();

    public List<ItemObject> GetItems()
    {
        return new List<ItemObject>(items);
    }

    public bool AddItem(ItemObject item)
    {
        if (items.Count < maxInventoryItems)
        {
            items.Add(item);
            return true;
        }

        return false;
    }

    public bool RemoveItem(ItemObject item)
    {
       return items.Remove(item);
    }

    public void ClearItemList()
    {
        items.Clear();
    }

    public ItemObject GetItemByName(string name)
    {
        foreach(ItemObject _item in items)
        {
            if (_item.name == name)
            {
                return _item;
            }
        }

        return null;
    }

    public ItemObject GetItemByID(int id)
    {
        foreach (ItemObject _item in items)
        {
            if (_item.id == id)
            {
                return _item;
            }
        }

        return null;
    }
}
