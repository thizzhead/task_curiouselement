using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Quest Object", menuName = "Quest")]
public class QuestObject : ScriptableObject
{
    public string title;
    [TextArea(5,15)]
    public string description;
    public ItemObject requiredItem;
    public ItemObject rewardedItem;
    [TextArea(5, 15)]
    public string dialogueFail;
    [TextArea(5, 15)]
    public string dialogueSuccess;
}
