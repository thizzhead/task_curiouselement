using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    [SerializeField]
    private PlayerController playerController;
    [SerializeField]
    private InputActionAsset inputActions;

    private void Awake()
    {
        inputActions["Interact"].performed += context => OnInteract(context);
        inputActions.Enable();
    }

    private void FixedUpdate()
    {
        playerController.lookDelta = inputActions["Look"].ReadValue<Vector2>();

        Vector2 movementInput = inputActions["Move"].ReadValue<Vector2>();
        playerController.movementDirection = new Vector3(movementInput.x, playerController.movementDirection.y, movementInput.y);
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        playerController.Interact();
    }

    public void DisableInputs()
    {
        inputActions.Disable();
    }
}
