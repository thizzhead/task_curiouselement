using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InventoryManager : MonoBehaviour
{
    public InventoryObject inventoryObject;
    public event System.Action<ItemObject> OnItemAdded;
    public event System.Action<ItemObject> OnItemRemoved;

    public void AddItem(ItemObject item)
    {
        inventoryObject.AddItem(item);
        OnItemAdded?.Invoke(item);
    }

    public void RemoveItem(ItemObject item)
    {
        inventoryObject.RemoveItem(item);
        OnItemRemoved?.Invoke(item);
    }

    private void OnApplicationQuit()
    {
        inventoryObject.ClearItemList();
    }
}
