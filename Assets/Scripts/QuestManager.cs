using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    [SerializeField]
    private List<Quest> quests = new List<Quest>();
    [SerializeField]
    private InventoryManager playerInventoryManager;
    [SerializeField]
    private DialogueManager dialogueManager;

    public event System.Action<Quest[]> OnQuestsUpdate;

    private void Start()
    {
        UpdateQuests(); 
    }

    public void UpdateQuests()
    {
        OnQuestsUpdate?.Invoke(quests.ToArray());
    }

    private void CompleteQuest(Quest quest, QuestObject questObject)
    {
        quest.completed = true;

        if (questObject.rewardedItem != null)
        {
            playerInventoryManager.AddItem(questObject.rewardedItem);

            if (questObject.requiredItem != null)
            {
                playerInventoryManager.RemoveItem(questObject.requiredItem);
            }
        }

        UpdateQuests();

        CheckAllQuestsCompleted();
    }

    private void CheckAllQuestsCompleted()
    {
        foreach(Quest quest in quests)
        {
            if (!quest.completed)
            {
                return;
            }
        }

        GameManager.Instance.CompleteGame();
    }

    public Quest GetQuestByQuestObject(QuestObject questObject)
    {
        foreach (Quest quest in quests)
        {
            if (quest.questObject == questObject)
            {
                return quest;
            }
        }

        return null;
    }

    public void VerifyQuestCompletion(QuestObject questObject)
    {
        Quest quest = GetQuestByQuestObject(questObject);

        if (quest != null)
        {
            if (!quest.completed)
            {
                ItemObject itemObject = null;

                if (quest.questObject.requiredItem == null)
                {
                    dialogueManager.StartDialogue(quest.questObject.dialogueSuccess, () => CompleteQuest(quest, questObject));
                    return;
                }
                else
                {
                    itemObject = playerInventoryManager.inventoryObject.GetItemByID(quest.questObject.requiredItem.id);
                }

                if (itemObject != null)
                {
                    dialogueManager.StartDialogue(quest.questObject.dialogueSuccess, () => CompleteQuest(quest, questObject));
                }
                else
                {
                    dialogueManager.StartDialogue(quest.questObject.dialogueFail, () => { });
                }
            }
        }
    }
}
