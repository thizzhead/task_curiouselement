using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HUDManager : MonoBehaviour
{
    [SerializeField]
    private Canvas hudCanvas;
    [SerializeField]
    private GameObject endGameScreenPrefab;

    [Header("Quests")]
    [SerializeField]
    private QuestManager questManager;
    [SerializeField]
    private RectTransform questUIPanel;
    [SerializeField]
    private GameObject questTextBoxPrefab;

    [Header("Inventory")]
    [SerializeField]
    private InventoryManager inventoryManager;
    [SerializeField]
    private GameObject inventoryPanel;
    [SerializeField]
    private GameObject inventorySlotPrefab;

    [Header("Dialogue")]
    [SerializeField]
    private DialogueManager dialogueManager;
    [SerializeField]
    private RectTransform dialogBox;
    [SerializeField]
    private RectTransform dialogueTextBox;

    public float dialogueBoxHeight = 175f;
    public float dialogueBoxAppearanceTime = 1f;
    public float dialogueBoxDisplayTime = 1f;

    private bool dialogueBoxDisplaying;
    private TMP_Text dialogueTextField;

    public event System.Action OnDialogueCompleted;

    private void Start()
    {
        dialogueTextField = dialogueTextBox.GetComponent<TMP_Text>();
    }

    private void OnEnable()
    {
        inventoryManager.OnItemAdded += AddInventoryItemToDisplay;
        inventoryManager.OnItemRemoved += RemoveItemFromInventoryDisplay;
        questManager.OnQuestsUpdate += UpdateQuestDisplay;
        GameManager.OnGameEnd += ShowEndGameScreen;
        dialogueManager.OnWriteDialogueText += WriteToDialogueBox;
    }

    private void AddInventoryItemToDisplay(ItemObject item)
    {
        GameObject inventoryItem = Instantiate(inventorySlotPrefab, inventoryPanel.transform);
        inventoryItem.name = item.name;
        Image inventoryItemIcon = inventoryItem.transform.Find("Icon").GetComponent<Image>();
        inventoryItemIcon.sprite = item.icon;
        inventoryItem.GetComponent<InventoryItem>().itemObject = item;
    }

    private void RemoveItemFromInventoryDisplay(ItemObject item)
    {
        foreach(RectTransform inventoryItemGameObject in inventoryPanel.transform)
        {
            if (inventoryItemGameObject.GetComponent<InventoryItem>().itemObject == item)
            {
                Destroy(inventoryItemGameObject.gameObject);
            }
        }
    }

    private void UpdateQuestDisplay(Quest[] quests)
    {
        foreach (RectTransform questObject in questUIPanel)
        {
            Destroy(questObject.gameObject);
        }

        foreach (Quest quest in quests)
        {
            GameObject questTextObject = Instantiate(questTextBoxPrefab);
            questTextObject.transform.name = "QuestTitle";
            questTextObject.transform.SetParent(questUIPanel, false);
            TMPro.TMP_Text questText = questTextObject.GetComponent<TMP_Text>();
            questText.text = quest.questObject.title;

            if (quest.completed)
            {
                questText.color = Color.green;
            }
        }
    }

    private void ShowEndGameScreen()
    {
        WriteToDialogueBox("You have finished all of your activities on your first day of work. Congratulations!");
        OnDialogueCompleted = ShowEndGameEffect;
    }

    private void ShowEndGameEffect()
    {
        GameObject endGameScreenObject = Instantiate(endGameScreenPrefab);
        endGameScreenObject.transform.SetParent(hudCanvas.transform, false);

        RectTransform endGameScreenObjectRect = endGameScreenObject.GetComponent<RectTransform>();

        RectTransform hudCanvasRect = hudCanvas.GetComponent<RectTransform>();

        Vector2 sizeTo = new Vector2(hudCanvasRect.rect.width, hudCanvasRect.rect.height);
        endGameScreenObjectRect.LeanSize(-sizeTo, 2f).setEaseOutBounce();
    }

    private void OnDialogueBoxDisplayComplete(string dialogueText)
    {
        StartCoroutine(RevealText(dialogueText));
    }

    private IEnumerator HideDialogueBox()
    {
        yield return new WaitForSeconds(dialogueBoxDisplayTime);
        dialogBox.LeanSize(new Vector2(dialogBox.rect.width, 0), dialogueBoxAppearanceTime).setEaseOutElastic();
        dialogueTextField.text = "";
        dialogueBoxDisplaying = false;
        OnDialogueCompleted?.Invoke();
    }

    public void WriteToDialogueBox(string dialogueText)
    {
        if (!dialogueBoxDisplaying)
        {
            dialogBox.LeanSize(new Vector2(dialogBox.rect.width, dialogueBoxHeight), dialogueBoxAppearanceTime).setEaseOutElastic().setOnComplete(() => OnDialogueBoxDisplayComplete(dialogueText));
            dialogueBoxDisplaying = true;
        }
    }

    private IEnumerator RevealText(string text)
    {
        int visibleCharacters = 0;

        while (visibleCharacters <= text.Length)
        {
            string displayedText = text.Substring(0, visibleCharacters);

            dialogueTextField.text = displayedText;

            visibleCharacters++;

            yield return new WaitForSeconds(0.03f);
        }
        StartCoroutine(HideDialogueBox());
    }
}
