using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueManager : MonoBehaviour
{
    [SerializeField]
    private HUDManager hudManager;
    private System.Action dialogueCompletedCallback;
    private bool dialogueFinished = true;

    public event System.Action<string> OnWriteDialogueText;

    private void Awake()
    {
        hudManager.OnDialogueCompleted += DialogueCompleted;
    }

    public void StartDialogue(string dialogueText, System.Action dialogueCompletedCallback)
    {
        if (dialogueFinished)
        {
            dialogueFinished = false;
            this.dialogueCompletedCallback = dialogueCompletedCallback;
            OnWriteDialogueText?.Invoke(dialogueText);
        }
    }

    private void DialogueCompleted()
    {
        dialogueCompletedCallback?.Invoke();
        dialogueCompletedCallback = null;
        dialogueFinished = true;
    }
}
