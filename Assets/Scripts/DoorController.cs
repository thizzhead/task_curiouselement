using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour, IInteractable
{
    [SerializeField]
    private ItemObject requiredItem;
    [SerializeField]
    private Transform hingeOrigin;

    private bool isOpen;
    private bool isAnimating;

    public void Interact(GameObject caller)
    {
        if (requiredItem != null)
        {
            InventoryManager inventoryManager = caller.GetComponent<InventoryManager>();
            DialogueManager dialogueManager = caller.GetComponent<DialogueManager>();

            if (inventoryManager != null)
            {
                ItemObject item = inventoryManager.inventoryObject.GetItemByID(requiredItem.id);

                if (item != null)
                {
                    ToggleDoor();
                }
                else
                {
                    
                    if (dialogueManager != null)
                    {
                        WriteMessageToHUD("You need an Entrance Card to enter.", dialogueManager);
                    }
                }
            }
        }
    }

    public void ToggleDoor()
    {
        if (!isAnimating)
        {
            isAnimating = true;
            if (!isOpen)
            {
                hingeOrigin.LeanRotateY(180f, 1f).setOnComplete(() => { isAnimating = false; isOpen = true; });
            }
            else
            {
                hingeOrigin.LeanRotateY(90f, 1f).setOnComplete(() => { isAnimating = false; isOpen = false; });
            }
        }
    }

    public void WriteMessageToHUD(string message, DialogueManager dialogueManager)
    {
        dialogueManager.StartDialogue(message, () => { });
    }
}
