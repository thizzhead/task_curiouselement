using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    public Camera playerCamera;
    public CharacterController characterController;
    public Vector3 movementDirection;
    public float movementSpeed = 10f;
    public Vector2 lookDelta;
    public float mouseSensitivity = 20f;
    public float minVerticalAngle = -90f;
    public float maxVerticalAngle = 90f;
    public float interactionDistance = 1.5f;

    private float cameraPitch;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        GameManager.Instance.player = this;
    }

    void Update()
    {
        RotateCamera(lookDelta);
        UpdateMovement();
    }

    public void UpdateMovement()
    {
        Vector3 movementVector = transform.TransformDirection(movementDirection) * movementSpeed;
        characterController.SimpleMove(movementVector);
    }

    public void RotateCamera(Vector2 rotation)
    {
        transform.Rotate(Vector3.up, rotation.x * mouseSensitivity * Time.deltaTime);

        cameraPitch -= rotation.y * mouseSensitivity * Time.deltaTime;

        cameraPitch = Mathf.Clamp(cameraPitch, minVerticalAngle, maxVerticalAngle);

        playerCamera.transform.localEulerAngles = Vector3.right * cameraPitch;
    }

    public void Interact()
    {
        RaycastHit hit;

        Ray cameraCenterRay = playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

        if (Physics.Raycast(cameraCenterRay, out hit, interactionDistance))
        {
            IInteractable interactableObject = hit.collider.transform.GetComponent<IInteractable>();

            if (interactableObject != null)
            {
                interactableObject.Interact(gameObject);
            }
        }
    }
}
