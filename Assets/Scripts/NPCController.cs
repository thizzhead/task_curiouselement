using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NPCController : MonoBehaviour, IInteractable
{
    [SerializeField]
    private QuestObject quest;

    public void Interact(GameObject caller)
    {
        if (quest != null)
        {
            QuestManager questManager = caller.GetComponent<QuestManager>();

            if (questManager != null)
            {
                questManager.VerifyQuestCompletion(quest);
            }
        }    
    }
}
