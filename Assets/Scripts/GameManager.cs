using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public PlayerController player;
    public static event System.Action OnGameEnd;

    private static GameManager _instance = null;
    public static GameManager Instance { 
        get
        {
            return _instance;
        }
    }

    public void CompleteGame()
    {
        OnGameEnd?.Invoke();
        player.GetComponent<InputManager>().DisableInputs();
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;

        DontDestroyOnLoad(gameObject);
    }
}
